import React, { Component } from 'react';
// import { Trans } from 'react-i18next';


function TableHeader(props) {
    const { data, title } = props;
    const { unit, left, right, center } = data || {};

    return (
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td className="vertical-align" align="left" rowspan="1" colspan="1" width="30%">
                        <div><b>{title}</b> <br /></div>
                        <div>Ban hành kèm theo Thông tư số 15/2018/TT-BKHCN ngày 15 tháng 11 năm 2018 <br /></div>
                        <div>Ngày nhận báo cáo: Ngày 15/02 năm 2022</div>
                    </td>
                    <td className="vertical-align" align="center" rowspan="1" colspan="3">
                        <div><b>{center}</b> <br /></div>
                        <div>(Có đến ngày 31/12 năm 2021)</div>
                    </td>
                    <td className="vertical-align" align="left" rowspan="1" colspan="3" width="30%">
                        <div><b>- Đơn vị báo cáo:</b> <br /></div>
                        <div>Sở KH&amp;CN Quảng Bình <br /></div>
                        <div><b>- Đơn vị nhận báo cáo:</b> <br /></div>
                        <div>Cục Thông tin KH&amp;CN quốc gia</div>
                    </td>
                </tr>
                <tr>
                    <td className="vertical-align" align="center" rowspan="1" colspan="4"></td>
                    <td className="vertical-align" align="center" rowspan="1" colspan="3"><i>Đơn vị tính: {unit}</i></td>
                </tr>
            </tbody>
        </table>
    );

}

export default TableHeader;