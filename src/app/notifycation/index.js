import React, { useState, useEffect } from 'react'
import { ProgressBar } from 'react-bootstrap';
import Modal from 'react-modal';
import { api, getUserId } from 'app/utils';
import { get, isEmpty } from 'lodash';
import moment from 'moment';
import { DATE_FORMAT_DD_MM_YYY } from 'app/constants';
import Spinner from 'app/shared/Spinner';

const imgSrc = require('../../assets/images/icon/notification.png');
const imgAdminSrc = require('../../assets/images/icon/admin.png');
const imgClockSrc = require('../../assets/images/icon/clock.png');
const imgTBSrc = require('../../assets/images/icon/thongbao.png');

var perChunk = 2// items per chunk    


const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    backgroundColor: '#9294a1'
  },

};


function Notifycation(props) {
  let subtitle;
  const [messagedLits, setMessagedResp] = useState([]);
  const [totalMessage, setTotalMessage] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [detailMessSelected, setDetailMessSelected] = useState(false);
  const [dataChunk, setDataChunk] = useState([]);


  const layout = [
    { i: 'a', x: 0, y: 0, w: 1, h: 2, static: true },
    { i: 'b', x: 1, y: 0, w: 3, h: 2, minW: 2, maxW: 4 },
    { i: 'c', x: 4, y: 0, w: 1, h: 2 }
  ];

  const userId = getUserId();

  const getMessageMe = async () => {
    try {
      setIsLoading(true);
      const res = await api(`auth/getAllMessage?userId=${userId}`);
      setMessagedResp(get(res, 'data.listMessage'));
      setTotalMessage(get(res, 'data.total'))
    } catch (e) {

    } finally {
      setIsLoading(false);
    }

  };

  function openModal(indexMess) {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = 'white';
  }

  function closeModal() {
    setIsOpen(false);
  }

  useEffect(() => {
    getMessageMe();
  }, []);


  useEffect(() => {
    if (messagedLits && messagedLits.length > 0) {
      let result = messagedLits.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / perChunk)

        if (!resultArray[chunkIndex]) {
          resultArray[chunkIndex] = [] // start a new chunk
        }

        resultArray[chunkIndex].push(item)

        return resultArray
      }, []);

      setDataChunk(result);
    }
  }, [messagedLits]);

  if (isLoading) {
    return <Spinner />
  }

  return (
    <div>
      <div className="page-header">
        <h3 className="page-title"> Thông báo </h3>
      </div>
      <div className="col-lg-12 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <h4 className="card-title">Bạn có ({totalMessage}) thông báo</h4>

            <div class="container">
              {dataChunk && dataChunk.map(array => {
                return (
                  <div class="row mt-4">
                    {array.map(mess => (
                      <div class="col-sm">


                        <div class="card p-2 pt-3">
                          <div className="d-flex align-middle align-center">
                            <div className="align-middle">
                              <img src={imgTBSrc} style={{ width: 130, height: 60 }} />
                            </div>
                            <div class="card-body">

                              <h5 class="card-title text-ellipsis">{get(mess, 'message.title')}</h5>
                              <p class="card-text text-ellipsis">{get(mess, 'message.content')}</p>
                              <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                                <div className="mt-1">
                                  <img src={imgAdminSrc} style={{ width: 16, height: 16 }} />
                                  <span className="ml-1">
                                    <small>  Admin</small>
                                  </span>
                                </div>
                                <div>
                                  <img src={imgClockSrc} style={{ width: 16, height: 16 }} />
                                  <span className="ml-1">
                                    <small>{moment(new Date(get(mess, 'message.updatedAt._seconds') * 1000)).format(DATE_FORMAT_DD_MM_YYY)}</small>
                                  </span>
                                </div>
                              </div>

                              <button onClick={() => {
                                setDetailMessSelected(mess)
                                openModal();
                              }} class="btn btn-primary mt-2">Xem chi tiết</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    ))}
                  </div>
                )
              })
              }

            </div>


          </div>
        </div>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <h4 ref={(_subtitle) => (subtitle = _subtitle)}>Chi tiết thông báo</h4>

        {detailMessSelected && <div className="overflow-auto">
          <h5 className="text-white">TIÊU ĐỀ: {get(detailMessSelected, 'message.title')}</h5>
          <h6 className="text-white">NỘI DUNG: {get(detailMessSelected, 'message.content')}</h6>
          <td>   <p className="text-white"> Admin gửi lúc: {moment(new Date(get(detailMessSelected, 'message.updatedAt._seconds') * 1000)).format(DATE_FORMAT_DD_MM_YYY)} </p></td>
        </div>}
        <button className="btn btn-danger" onClick={closeModal}>Đóng</button>
      </Modal>
    </div>
  )
}

export default Notifycation;
