import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { Dropdown } from 'react-bootstrap';
import { Trans } from 'react-i18next';
import moment from 'moment';
import { actionLogOut } from 'app/redux/actions';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { get, isEmpty } from 'lodash';
import { useToken } from 'app/hooks';
import { api, getUserId } from 'app/utils';
import { DATE_FORMAT_DD_MM_YYY } from 'app/constants';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    backgroundColor: '#9294a1'
  },

};



function Navbar(props) {
  const { actionLogOut, history } = props;
  const { setToken } = useToken();
  const userId = getUserId();
  let subtitle;

  const [messagedLits, setMessagedResp] = useState([]);
  const [totalMessage, setTotalMessage] = useState(0);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [detailMessSelected, setDetailMessSelected] = useState(false);

  const toggleOffcanvas = () => {
    document.querySelector('.sidebar-offcanvas').classList.toggle('active');
  }
  const toggleRightSidebar = () => {
    document.querySelector('.right-sidebar').classList.toggle('open');
  }

  const onLogOut = (event) => {
    event.preventDefault();
    actionLogOut();
    setToken({});
    history.replace('/login')
  }



  function openModal(indexMess) {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = 'white';
  }

  function closeModal() {
    setIsOpen(false);
  }


  const getMessageMe = async () => {
    const res = await api(`auth/getAllMessage?userId=${userId}`);
    setMessagedResp(get(res, 'data.listMessage'));
    setTotalMessage(get(res, 'data.total'))
  };

  useEffect(() => {
    getMessageMe();
  }, [])

  return (
    <nav className="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
      <div className="navbar-menu-wrapper d-flex align-items-center justify-content-between">
        <a className="navbar-brand brand-logo-mini align-self-center d-lg-none" href="!#" onClick={evt => evt.preventDefault()}><img src={require("../../assets/images/logo-mini.svg")} alt="logo" /></a>
        <button className="navbar-toggler navbar-toggler align-self-center" type="button" onClick={() => document.body.classList.toggle('sidebar-icon-only')}>
          <i className="mdi mdi-menu"></i>
        </button>
        <ul className="navbar-nav navbar-nav-left header-links align-self-center">
          <li className="nav-item font-weight-semibold d-none  d-md-flex">
            SỞ KHOA HỌC CÔNG VÀ NGHỆ TỈNH QUẢNG BÌNH
            <br />
            BÁO CÁO THỐNG KÊ KHOA HỌC VÀ CÔNG NGHỆ TRỰC TUYẾN
          </li>

        </ul>

        <ul className="navbar-nav navbar-nav-right">
          <li className="nav-item  nav-profile border-0">
            <Dropdown>
              <Dropdown.Toggle className="nav-link count-indicator p-0 toggle-arrow-hide bg-transparent">
                <i className="mdi mdi-email-outline"></i>
                <span className="count">{totalMessage}</span>
              </Dropdown.Toggle>
              <Dropdown.Menu className="navbar-dropdown preview-list">
                <Dropdown.Item className="dropdown-item  d-flex align-items-center" onClick={evt => evt.preventDefault()}>
                  <p className="mb-0 font-weight-medium float-left">{!isEmpty(messagedLits) ? 'Tất cả thông báo' : 'Bạn chưa có thông báo nào'} </p>
                  {/* <span className="badge badge-pill badge-primary">{!isEmpty(messagedLits) ? 'Tất cả thông báo' : 'Bạn chưa có thông báo nào'}</span> */}
                </Dropdown.Item>
                <div className="dropdown-divider"></div>
                {messagedLits && messagedLits.map((mess, idx) => {
                  return (
                    <>
                      <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center" onClick={() => {
                        setDetailMessSelected(mess)
                        openModal();
                      }}>
                        <div className="preview-thumbnail">
                          <img src={require("../../assets/images/icon/notification.png")} alt="profile" className="img-sm profile-pic" style={{ width: 18, height: 30 }} /> </div>
                        <div className="preview-item-content flex-grow py-2">
                          <p className="preview-subject ellipsis font-weight-medium text-dark"><Trans>{get(mess, 'message.title')}</Trans> </p>
                          <p className="font-weight-light ellipsis small-text"> <Trans>{get(mess, 'message.content')}</Trans> </p>
                          <p className="font-weight-light small-text"> <Trans>{moment(get(mess, 'message.updatedAt')).format(DATE_FORMAT_DD_MM_YYY)}</Trans> </p>
                        </div>
                      </Dropdown.Item>
                      <div className="dropdown-divider"></div>
                    </>
                  )
                })}

              </Dropdown.Menu>
            </Dropdown>
          </li>


          <li className="nav-item  nav-profile border-0">
            <Dropdown>
              {/* <Dropdown.Toggle className="nav-link count-indicator bg-transparent"> */}
              <Link className="nav-link" to={{
                pathname: `/user/profile`,
              }}>
                <img className="img-xs rounded-circle" src={require("../../assets/images/faces/facePlaceHolder.png")} alt="Profile" />
                {/* </Dropdown.Toggle> */}
              </Link>
              <Dropdown.Menu className="preview-list navbar-dropdown pb-3">
                <Dropdown.Item className="dropdown-item p-0 preview-item d-flex align-items-center border-bottom" href="!#" onClick={evt => evt.preventDefault()}>
                  <div className="d-flex">
                    <div className="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                      <i className="mdi mdi-account-outline mr-0">Xem thông tin</i>
                    </div>
                  </div>
                </Dropdown.Item>
                {/* <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center border-0 mt-2" onClick={evt => evt.preventDefault()}>
                    <Trans>Manage Accounts</Trans>
                  </Dropdown.Item>
                  <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center border-0" onClick={evt => evt.preventDefault()}>
                    <Trans>Change Password</Trans>
                  </Dropdown.Item>
                  <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center border-0" onClick={evt => evt.preventDefault()}>
                    <Trans>Check Inbox</Trans>
                  </Dropdown.Item> */}
                <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center border-0" onClick={event => onLogOut(event)}>
                  <Trans>Đăng xuất</Trans>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </li>
        </ul>
        <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" onClick={toggleOffcanvas}>
          <span className="mdi mdi-menu"></span>
        </button>

        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h4 ref={(_subtitle) => (subtitle = _subtitle)}>Chi tiết thông báo</h4>

          <div>
            <h5 className="text-white">TIÊU ĐỀ: {get(detailMessSelected, 'message.title')}</h5>
            <h6 className="text-white">NỘI DUNG: {get(detailMessSelected, 'message.content')}</h6>
            <td>   <p className="text-white"> Admin gửi lúc: {moment(new Date(get(detailMessSelected, 'message.updatedAt._seconds') * 1000)).format(DATE_FORMAT_DD_MM_YYY)} </p></td>
          </div>
          <button className="btn btn-danger" onClick={closeModal}>Đóng</button>
        </Modal>
      </div>
    </nav>
  );
}

const mapStateToProps = state => ({
  authState: get(state, 'authState'),
});

const mapDispatchToProps = {
  actionLogOut,
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar))
