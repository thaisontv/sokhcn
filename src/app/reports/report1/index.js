import React, { useEffect, useState } from 'react'
import { useForm } from "react-hook-form";
import {
  CommonTableHeaderReport,
  CommonTableFooterReport, HeaderExport,
  FooterUpload, inputStyle
} from 'app/components';
import { get } from 'lodash';
import { getUserId } from 'app/utils';
import { connect } from 'react-redux';
import { actionGetStorageReportDetail } from 'app/redux/actions';
import { api } from 'app/utils';
import Spinner from 'app/shared/Spinner';
import parse from 'html-react-parser';
import * as FileSaver from 'file-saver';
import { useRouter } from 'app/hooks';
import { Routers } from 'app/constants';

function ReportDetail(props) {

  const { location, actionViewtemplate, reportState, tableData,
    idReportUser, isGetttingReportDetail, reportDetailTitle,
    reportDetailHeader, reportDetailFooter } = props;

  const router = useRouter();

  const { idReport, year, followUserId } = get(location, 'state', {});

  if (!idReport) {
    router.push(Routers.REPORT)
  }
  const isLoadingUploadFile = get(reportState, 'isLoadingUploadFile');

  const userId = followUserId ? followUserId : getUserId();

  const [dynamicTableData, setDynamicTableData] = useState(tableData);
  const idxRowsDisabled = [0];
  const [exportting, setExportting] = useState(false);
  const { register, handleSubmit, watch, formState: { errors }, setValue, getValues } = useForm();


  const onSubmit = async () => {
    const reqData = { userId, idReportUser, rawData: dynamicTableData };
    api('reports/updateDataSaved', reqData);
  }

  const onChangeTimeFooter = ({ dateValue = '01', monthValue = '01', yearValue = '2021' }) => {
    api('reports/updateDateMonthSaved', { userId, idReportUser, date: dateValue, month: monthValue, year: yearValue });
  }

  const inputChange = async ({ event, id, idxRow, idxCol }) => {
    const valueText = event.target.value ? parseInt(event.target.value) : 0;
    let change = [...dynamicTableData];
    setValue(id, valueText);
    change[idxRow].rowCols[idxCol].value = valueText;

    let sumRow = 0;
    for (let i = 3; i <= 6; i++) {
      sumRow += parseInt(change[idxRow].rowCols[i].value);
      setValue(change[idxRow].rowCols[2].id, sumRow);
      change[idxRow].rowCols[2].value = sumRow;
    }

    let sumCol1 = 0, sumCol2 = 0, sumCol3 = 0, sumCol4 = 0, sumCol5 = 0;
    for (let i = 2; i < change.length; i++) {
      if (change[i].rowCols[2].type === 'input') {
        sumCol1 += parseInt(change[i].rowCols[2].value);
        sumCol2 += parseInt(change[i].rowCols[3].value);
        sumCol3 += parseInt(change[i].rowCols[4].value);
        sumCol4 += parseInt(change[i].rowCols[5].value);
        sumCol5 += parseInt(change[i].rowCols[6].value);
      }
    }
    setValue(change[0].rowCols[2].id, sumCol1);
    change[0].rowCols[2].value = sumCol1;

    setValue(change[0].rowCols[3].id, sumCol2);
    change[0].rowCols[3].value = sumCol2;

    setValue(change[0].rowCols[4].id, sumCol3);
    change[0].rowCols[4].value = sumCol3;

    setValue(change[0].rowCols[5].id, sumCol4);
    change[0].rowCols[5].value = sumCol4;

    setValue(change[0].rowCols[6].id, sumCol5);
    change[0].rowCols[6].value = sumCol5;

    //save to DB
    setDynamicTableData(change);
    const reqData = { valueText, userId, idReportUser, rawData: change };
    api('reports/updateDataSaved', reqData);
  }

  const onExportExcel = async () => {
    try {
      setExportting(true);
      const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      const fileExtension = '.xlsx';
      const fileName = 'Bieu01_KHCN_TC';
      const response = await api('reports/exportExcel', {
        dataList: dynamicTableData,
        idReport: idReport
      });
      const buffer = get(response, 'data.buffer.data');
      const arr = new Uint8Array(buffer);
      const dataFile = new Blob([arr], { type: fileType });
      FileSaver.saveAs(dataFile, fileName + fileExtension);
    } catch (e) {
      console.log('onExportExcel error', e)
    }
    finally {
      setExportting(false);
    }
  }

  const analysisReport = async () => {
    try {
      setExportting(true);
      let sumInput = 0, sumTyped = 0;
      let ratio = 0;
      if (!dynamicTableData) return;
      dynamicTableData.map(row => {
        row.rowCols.map(col => {
          const { type, value } = col;
          if (type === 'input' && value > 0) {
            sumTyped++;
          }
          if (type === 'input') {
            sumInput++;
          }
        })
      });
      if (sumTyped !== 0){
        ratio = Number(sumTyped / sumInput);
      }
      const data = await api('reports/analysisByUser', {
        userId: userId,
        idReport: idReport,
        ratio: ratio,
        year: year
      });
      return data;
    } catch (e) {

    } finally {
      setExportting(false);
    }
  }

  const onFinishReport = async () => {
    await analysisReport();
    router.push("/report")
  }

  useEffect(() => {
    if (tableData) {
      setDynamicTableData(tableData);
    }
  }, [tableData]);


  useEffect(() => {
    actionViewtemplate({ idReport, year, userId });
    //analysisReport();
  }, []);


  if (isGetttingReportDetail || exportting || isLoadingUploadFile) {
    return <Spinner />
  }

  return (
    <div>
      <div className="page-header">
        <h3 className="page-title"> {reportDetailTitle}</h3>
      </div>
      <div className="row">
        <div className="col-lg-12 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <HeaderExport onExportExcel={() => onExportExcel()} />
              <div className="table-responsive">
                <CommonTableHeaderReport data={reportDetailHeader} title={reportDetailTitle} />
                <form onSubmit={handleSubmit(onSubmit)}>
                  <table className="table table-bordered">
                    <tbody>
                      <tr>
                        <td className="vertical-align" align="center" rowspan="3" colspan="1" width="30%"></td>
                        <td className="vertical-align" align="center" rowspan="3" colspan="1" width="10%">Mã số</td>
                        <td className="vertical-align" align="center" rowspan="3" colspan="1">Tổng số</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="4">Chia theo loại hình kinh tế</td>
                      </tr>
                      <tr>
                        <td className="vertical-align" align="center" rowspan="1" colspan="2">Nhà nước</td>
                        <td className="vertical-align" align="center" rowspan="2" colspan="1">Ngoài nhà nước</td>
                        <td className="vertical-align" align="center" rowspan="2" colspan="1">Có vốn đầu tư <br />nước ngoài</td>
                      </tr>
                      <tr>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">Trung ương</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">Địa phương</td>
                      </tr>
                      <tr>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">A</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">B</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">1</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">2</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">3</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">4</td>
                        <td className="vertical-align" align="center" rowspan="1" colspan="1">5</td>
                      </tr>


                      {tableData && tableData.map((row, idxRow) => {
                        return (
                          <tr>
                            {get(row, 'rowCols', []).map((col, idxCol) => {
                              const { id, type } = col || {};
                              if (type === 'title') return (<td className="vertical-align" rowspan="1" colspan="1" >{parse(get(col, 'title'))}</td>);
                              if (type === 'code') return (<td className="vertical-align" rowspan="1" align="center" colspan="1">{get(col, 'title')}</td>);
                              if (type === 'input') {
                                return (
                                  <td className="vertical-align" align="center" rowspan="1" colspan="1">
                                    <input type="number"
                                      readOnly={idxRowsDisabled.includes(idxRow) || idxCol === 2 || userId === 'admin' || followUserId}
                                      {...register(get(col, 'id'))}
                                      name={get(col, 'id')}
                                      min={0}
                                      id={get(col, 'id')}
                                      class="form-control-sm form-control form-input"
                                      style={inputStyle}
                                      defaultValue={get(col, 'value', 0)}
                                      onChange={event => inputChange({ event, id, idxRow, idxCol })}
                                    />
                                  </td>
                                )
                              }
                              return (<td></td>);

                            })}
                          </tr>

                        )
                      })}

                    </tbody>
                  </table>
                </form>
                <CommonTableFooterReport data={reportDetailFooter} onChangeTime={onChangeTimeFooter} />
                <FooterUpload onFinishReport={onFinishReport} storageID={idReportUser}
                  fileUploaded={get(reportState, 'reportDetail.data.fileUpload')}
                  onFinishUpload={() => actionViewtemplate({ idReport, year, userId })}
                />
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  )
}


const mapStateToProps = state => ({
  authState: get(state, 'authState'),
  reportState: get(state, 'reportState'),
  tableData: get(state, 'reportState.reportDetail.data.rawData', []),
  sizeCols: get(state, 'reportState.reportDetail.data.size', 1),
  idReportUser: get(state, 'reportState.reportDetail.data.id'),
  isGetttingReportDetail: get(state, 'reportState.isGetttingReportDetail'),
  reportDetailHeader: get(state, 'reportState.reportDetail.data.headers', {}),
  reportDetailTitle: get(state, 'reportState.reportDetail.data.title', ''),
  reportDetailFooter: get(state, 'reportState.reportDetail.data.footer', {}),

});

const mapDispatchToProps = {
  actionViewtemplate: actionGetStorageReportDetail
};


export default connect(mapStateToProps, mapDispatchToProps)(ReportDetail)
