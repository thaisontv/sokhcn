import * as yup from "yup";

export const LoginSchema = yup.object({
    userName: yup.string().required('Nhập tài khoản đăng nhập'),
    password: yup.string().required('Nhập mật khẩu'),
}).required();