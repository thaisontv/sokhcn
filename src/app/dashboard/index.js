import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { get, isEmpty } from 'lodash';
import { actionGetUserMetadata, actionGetAnalysis, actionGetWorkspace, actionGetAllRole, actionGetAllReports } from 'app/redux/actions';
import { withRouter } from 'react-router-dom';
import Spinner from 'app/shared/Spinner';
import { ProgressBar } from 'react-bootstrap';
import { api } from 'app/utils';

const options =
{
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  },
  legend: {
    display: false
  },
  elements: {
    point: {
      radius: 0
    }
  }

};


function DashBoard(props) {

  const { authState, actionGetUserMetadata, actionGetAnalysis, history, reportsState } = props || {};
  const { isGetttingAnalysis } = reportsState || false;

  const [analysisData, setAnalysisData] = useState([]);
  const [barData, setBarData] = useState([]);



  useEffect(() => {
    const userInfo = get(authState, 'userInfo');
    if (!userInfo) {
      const userIdString = localStorage.getItem('userId');
      const { userId } = JSON.parse(userIdString);
      if (!userId) {
        history.replace('/');
      }
      actionGetAnalysis({ userId });
      actionGetUserMetadata(userId);
      actionGetWorkspace(userId);
      actionGetAllRole(userId);
      actionGetAllReports(userId);
    }
  }, []);

  const dataBarChart = {
    labels: ["2019", "2020", "2021"],
    datasets: [{
      label: '% Hoàn thành',
      data: [10, 19, 3,],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1,
      fill: false,
      barPercentage: 0.5,
      stack: 'fafd'
    }]
  };

  const options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false,
    },
    elements: {
      point: {
        radius: 0
      }
    },

  };


  if (isGetttingAnalysis) {
    return <Spinner style={{ position: null }} />;
  }

  return (
    <div>
      <div className="page-header">
        <h3 className="page-title">
          Tổng quan số liệu
        </h3>
      </div>
      <div className="row">
        <div className="col-md-12 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Thống kê số liệu theo năm</h4>
              <Bar data={dataBarChart}
                options={options}
              />
            </div>
          </div>
        </div>

        <div className="col-lg-12 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Thống kê số liệu theo báo cáo</h4>
              <div className="table-responsive">
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th> User </th>
                      <th> First name </th>
                      <th> Progress </th>
                      <th> Amount </th>
                      <th> Deadline </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face1.jpg")} alt="user icon" />
                      </td>
                      <td> Biểu 1 </td>
                      <td>
                        <ProgressBar variant="success" now={25} />
                      </td>
                      <td> 25% </td>
                      <td> May 15, 2015 </td>
                    </tr>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face2.jpg")} alt="user icon" />
                      </td>
                      <td> Biểu 2 </td>
                      <td>
                        <ProgressBar variant="danger" now={75} />
                      </td>
                      <td> 75%</td>
                      <td> July 1, 2015 </td>
                    </tr>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face3.jpg")} alt="user icon" />
                      </td>
                      <td> Biểu 3</td>
                      <td>
                        <ProgressBar variant="warning" now={90} />
                      </td>
                      <td>90% </td>
                      <td> Apr 12, 2015 </td>
                    </tr>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face4.jpg")} alt="user icon" />
                      </td>
                      <td> Biểu 4 </td>
                      <td>
                        <ProgressBar variant="primary" now={50} />
                      </td>
                      <td> 50%</td>
                      <td> May 15, 2015 </td>
                    </tr>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face5.jpg")} alt="user icon" />
                      </td>
                      <td> Biểu 5</td>
                      <td>
                        <ProgressBar variant="danger" now={60} />
                      </td>
                      <td> 60%</td>
                      <td> May 03, 2015 </td>
                    </tr>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face6.jpg")} alt="user icon" />
                      </td>
                      <td> Biểu 6 </td>
                      <td>
                        <ProgressBar variant="info" now={65} />
                      </td>
                      <td> 65%</td>
                      <td> April 05, 2015 </td>
                    </tr>
                    <tr>
                      <td className="py-1">
                        <img src={require("../../assets/images/faces/face7.jpg")} alt="user icon" />
                      </td>
                      <td>Biểu 7 </td>
                      <td>
                        <ProgressBar variant="warning" now={20} />
                      </td>
                      <td> $ 150.00 </td>
                      <td> June 16, 2015 </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  )
}


const mapStateToProps = state => ({
  authState: get(state, 'authState'),
  reportsState: get(state, 'reportState'),
});

const mapDispatchToProps = {
  actionGetUserMetadata,
  actionGetAnalysis,
  actionGetWorkspace,
  actionGetAllReports
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DashBoard))
