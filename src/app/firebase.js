
import firebase from 'firebase';
import { firebaseConfig } from './configs';

firebase.initializeApp(firebaseConfig);
var storage = firebase.storage();
export default storage;