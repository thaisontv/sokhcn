export const getUserId = () => {
    const userIdString = localStorage.getItem('userId');
    const userId = JSON.parse(userIdString);
    return userId?.userId
};