## Báo cáo thống kê KHCN tỉnh Quảng Bình

### tài liệu mô tả cho máy tính đã cài đặt các môi trường của node.js, firebase client
- Nếu chưa cài đặt, vui lòng tham khảo các link sau đề tiến hành cài đặt môi trường

1. node.js: https://nodejs.org/en/download/
2. npm client: https://firebase.google.com/docs/cli 

##  Run local

** Các bước run project
** 1.clone code** <br/>
** 2.cd vào thư mục vừa clone.** <br/>
** 3. dùng command line run: **$npm install <br/>
** 4. $npm start <br/>****

Web sẽ chạy ở chế đố debug localhost.<br>
Open [http://localhost:3000](http://localhost:3000)  cổng mặc định


## Deploy
Build Web
Run lệnh sau: **$npm run build**<br/>
$**firebase deploy --only hosting**
 
## Account test
Role admin: 
user name: admin
passwword: admin@123

Role reporter
user name: sonthai
password: khcn123

link portal: https://khcnquangbinh.xyz/ <br/>
link firebase data base: https://console.firebase.google.com/u/0/project/reporthccn/overview

